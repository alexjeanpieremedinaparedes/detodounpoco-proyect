import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ReportesComponent } from './reportes/reportes.component';
import { VentasComponent } from './ventas/ventas.component';
import { AdminestrucutainicialComponent } from 'src/app/layouts/admin/adminestrucutainicial/adminestrucutainicial.component';
import { InicioadminComponent } from './inicioadmin/inicioadmin.component';

const routes: Routes = [

  {path:'',component:AdminestrucutainicialComponent,

    children:[
      { path: 'inicio', component: InicioadminComponent},
      { path: 'reportes', component: ReportesComponent},
      { path: 'ventas', component: VentasComponent},
      { path: '', pathMatch:'full', redirectTo:'inicio'}
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
