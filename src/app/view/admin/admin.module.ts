import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioadminComponent } from './inicioadmin/inicioadmin.component';
import { ReportesComponent } from './reportes/reportes.component';
import { VentasComponent } from './ventas/ventas.component';
import { AdminestrucutainicialComponent } from 'src/app/layouts/admin/adminestrucutainicial/adminestrucutainicial.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { AdminRoutingModule } from './adminrouting.module';
import { IniciowebComponent } from '../web/inicioweb/inicioweb.component';



@NgModule({
  declarations: [
    AdminestrucutainicialComponent,
      InicioadminComponent,
      ReportesComponent,
      VentasComponent

  ],
  imports: [
    CommonModule,
    SharedModule,
    // Modulo para que funcione el del router-outlet
    RouterModule,
    // Ruta de admin
    AdminRoutingModule
  ],

  exports:[
    InicioadminComponent,
    ReportesComponent,
    VentasComponent,
    AdminestrucutainicialComponent
  ]

})
export class AdminModule { }
