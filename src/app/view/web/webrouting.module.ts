import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { IniciowebComponent } from './inicioweb/inicioweb.component';
import { ContactosComponent } from './contactos/contactos.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { WebestrucutainicialComponent } from 'src/app/layouts/web/webestrucutainicial/webestrucutainicial.component';


const routes: Routes = [
  { path: '', component: WebestrucutainicialComponent,
    children:[
      { path: 'inicio', component:IniciowebComponent },
      { path: 'contactos', component: ContactosComponent },
      { path: 'nosotros', component: NosotrosComponent },
      { path:'', pathMatch:'full', redirectTo:'inicio'}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class webRoutingModule {}
