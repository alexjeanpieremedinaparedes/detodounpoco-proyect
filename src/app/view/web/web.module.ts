import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactosComponent } from './contactos/contactos.component';
import { IniciowebComponent } from './inicioweb/inicioweb.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { WebestrucutainicialComponent } from 'src/app/layouts/web/webestrucutainicial/webestrucutainicial.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { webRoutingModule } from './webrouting.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CoreModule } from 'src/app/core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { DemoMaterialModule } from 'src/app/material-module';



@NgModule({
  declarations: [
    ContactosComponent,
    IniciowebComponent,
    NosotrosComponent,
    WebestrucutainicialComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    // Modulo para que funcione el del router-outlet
    RouterModule,
    // Ruta de web
    webRoutingModule,
     // Spinner
     NgxSpinnerModule,
     // Modulo
     CoreModule,
     // svg-icon
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    // Angular Material
    DemoMaterialModule
  ],
  exports:[
    WebestrucutainicialComponent,
    IniciowebComponent,
    NosotrosComponent,
    ContactosComponent
  ]
})
export class WebModule { }
