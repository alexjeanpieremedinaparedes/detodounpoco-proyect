import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';

const routes: Routes = [

  { path: 'login', component: LoginComponent},

  { path: 'register', component: RegisterComponent },

  { path: 'web', loadChildren:() => import('./view/web/web.module').then((m)=>m.WebModule)},

  { path: 'admin', loadChildren:() => import('./view/admin/admin.module').then((m)=>m.AdminModule)},

  { path:'**',
    redirectTo:'login',
    pathMatch:'prefix'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
