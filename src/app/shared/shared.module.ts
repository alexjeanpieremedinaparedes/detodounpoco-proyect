import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSpinnerModule } from 'ngx-spinner';
import { FooteradminComponent } from './footeradmin/footeradmin.component';
import { NavbaradminComponent } from './navbaradmin/navbaradmin.component';
import { FooterwebComponent } from './footerweb/footerweb.component';
import { NavbarwebComponent } from './navbarweb/navbarweb.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SidebarwebComponent } from './sidebarweb/sidebarweb.component';
import { SidebaradminComponent } from './sidebaradmin/sidebaradmin.component';


@NgModule({
  declarations: [
    FooteradminComponent,
    NavbaradminComponent,
    FooterwebComponent,
    NavbarwebComponent,
    SidebarwebComponent,
    SidebaradminComponent,


  ],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),

  ],
  exports:[
    FooteradminComponent,
    NavbaradminComponent,
    FooterwebComponent,
    NavbarwebComponent,
    SidebarwebComponent,
    SidebaradminComponent
  ]
})
export class SharedModule { }
