import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sidebarweb',
  templateUrl: './sidebarweb.component.html',
  styleUrls: ['./sidebarweb.component.css']
})
export class SidebarwebComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  onNosotros(){
    this.router.navigateByUrl('web/contactos');
  }


}
