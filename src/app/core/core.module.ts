import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './components/loading/loading.component';
import { QuestionComponent } from './components/question/question.component';
import { NgxSpinnerModule } from 'ngx-spinner';



@NgModule({
  declarations: [
    LoadingComponent,
    QuestionComponent
  ],
  imports: [
    CommonModule,
    NgxSpinnerModule
  ],
  exports:[
    LoadingComponent,
    QuestionComponent
  ]
})
export class CoreModule { }
