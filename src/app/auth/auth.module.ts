import { NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { webRoutingModule } from '../view/web/webrouting.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppModule } from '../app.module';
import { AdminModule } from '../view/admin/admin.module';
import { WebModule } from '../view/web/web.module';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],

  imports: [
    CommonModule,
    // svg-icon
    HttpClientModule, AngularSvgIconModule.forRoot(),
    // Modulo para que funcione el del router-outlet
    RouterModule,
    AppRoutingModule,
    // Spinner
    NgxSpinnerModule,
    // Modulo
    CoreModule,
    SharedModule,
    AdminModule,
    WebModule


  ],

  exports:[
    LoginComponent,
    RegisterComponent
  ]

})
export class AuthModule { }
